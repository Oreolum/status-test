
import { IItem } from './TreeStore.interface'

export class TreeStore {

    items: IItem[];

    constructor(items: IItem[]) {
        this.items = items;
    }

    getAll() {
        return this.items
    }

    getItem(id: string | number) {
        const index = this.items.findIndex(item => item.id == id)
        return this.items[index]
    }

    getChildren(id: string | number) {
        return this.items.filter(item => item.parent == id)
    }

    getAllChildren(id: string | number) {
        let childrens: IItem[] = []
        const recurse = (id: string | number) => {
            const items = this.items.filter(item => item.parent == id)
            if (items.length) {
                childrens = childrens.concat(items)
                items.forEach(item => recurse(item.id))
            }
        }
        recurse(id)
        return childrens
    }

    getAllParents(id: string | number) {
        const parents: IItem[] = []
        let parent = id
        let currentItem: IItem | undefined
        do {
            currentItem = this.items.find(item => item.id == parent)
            parent = currentItem ? currentItem.parent : ''
            currentItem && parents.push(currentItem)
        } while (parent && parent !== 'root')
        return parents
    }
}
