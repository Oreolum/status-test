# status-test

This template should help get you started developing with Vue 3 in Vite.

### Project Install

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Compile and Minify for Production

```sh
npm run build
```
